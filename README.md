# About Guidelines

We all want to hear "Good job!". But what is "good enough"? The word "Good" is very ambiguous here. It must be defined.

You might be contributing in Engineering, Marketing, Sales, Product Design, HR at Smarter.Codes. Whatever task you are doing, there
could be 10 ways to do the same thing - 5 good ways, and 5 bad ways. **Guidelines is a place where you can enlist the 'good ways' and 'bad ways' to do the same thing.**

You can use guidelines to define 'the standard' to others (or learn from them) on what qualifies as good, and what qualifies as bad practice.

We seek [everyone in Smarter.Codes investing 10-25% of their time weekly contributing to Guidelines](https://wiki.smarter.codes/objectives-in-2021/work-with-world-class-standards/). Inside Time Tracker (hubstaff) please log your time in this project named 'Guidelines'.

# Contributing

Smarter.Codes is like a democracy. So YOU must define what must be treated as "good practice" at Smarter.Codes.
For each practice (good or bad) we have tried to create [an issue](https://docs.gitlab.com/ee/user/project/issues/). 
We call them 'guideline issue'. Contributing your own guideline issue requires patience.
Your new guideline would either improve an existing guideline, or it would be posted as a whole new guideline.
Before contributing it becomes important to spend time to see what already has been contributed.
This takes us to our first step - of navigating to the breadth and depth of guidelines before you make your contribution.

## 1. Pick a category for your guideline
There are 100+ guidelines, so we have [categorized them in groups and projects](https://gitlab.com/smarter-codes/guidelines).
We also call them categories. You will come across groups/projects which are
|Named After|Example|Description|
|-----------|--------|-----------|
|A Tool     |[Python](https://gitlab.com/tushar-sandbox/general-programming-guidelines/languages/python)| These categories are often leaf categories. No child categories exist under them|
|A Job Role |[Frontend Coding](https://gitlab.com/smarter-codes/guidelines/software-engineering/coding/frontend)| These categories are often parent to Tool categories above.
|A Job Title|[Software Engineering](https://gitlab.com/smarter-codes/guidelines/software-engineering)|These categories are often parent to Job Role categories above.|
|About [something here]|[About Software Engineering](https://gitlab.com/smarter-codes/guidelines/software-engineering/about-software-engineering), [About Coding](https://gitlab.com/smarter-codes/guidelines/software-engineering/coding/about-coding), [About Deploy](https://gitlab.com/smarter-codes/guidelines/software-engineering/deploy/about-deploy), [About Frontend](https://gitlab.com/smarter-codes/guidelines/software-engineering/coding/frontend/about-frontend)| While digging into groups if you see a project named "About [someting here]" - open that first. Such projects are like 'README files' of that vast group you are about to enter. |

Pick the category where you think your guideline belongs. Before contributing, please spend time going deep under these groups and projects<sup>please keep aside 30 mins if you are just starting out</sup>. Because our guidelines are work in progress you might easily come across a situation when no category already exists.
Create a new group/project yourself if it does not exists already. When creating groups/projects please discuss it first with others in an issue<sup>[See Example](https://gitlab.com/smarter-codes/guidelines/software-engineering/programming-foundations/-/issues/3)</sup>. Message [maintainers](#maintainers) listed below to speed up moderation.

## 2. Writing your guideline.
1. Ensure [Guideline does only 1 thing](#7)
2. Talk about [WHY the guideline is needed](#3).
3. See [HOW others in the world are rather doing it](#4).
4. Finally [Write guideline in Markdown, an Issue, or a Wiki. Link them together](#5).  

## 3. Keep referring the guideline to your colleagues.
Guidelines become culture in Smarter.Codes only if we talk about these guidelines every now and then. We are also trying to encourage "continuous feedback" as a culture in Smarter.Codes. So we encourage everyone to 'review work' of each once every day (worst case - twice a week). As a programmer when you are doing a code review, or as a marketer you are reviewing the work of your colleague ([nope, we don't have continuous reviews happening in my team](https://gitlab.com/smarter-codes/guidelines/okr/-/blob/master/weekly-reviews.md)) - you might find your colleague following a bad practice - share with them deep link to very guideline here which you think we must comply.

## 4. Keep calm, and improve :-)  
If you received link to a guideline from your colleague, because in their opinion you have not followed some guideline then work with them to either improve your work, or work with them to improve the guideline. Just improve. Do not take it personally. The feedback you received was not about you, it was rather about work done at Smarter.Codes. Kindly turn yourself into a seeker - a learner forever. Especially if you receive feedback from someone who is same in experience or lesser in experience from you - it is important for you to preserve the culture of Smarter.Codes where we aspire to be 'lifelong learners', no matter if learning is coming in from someone more experienced than us, or someone less experienced than us.

# Maintainers
@tushar12123, @rickysb  
We are seeking more maintainers. To become one of the maintainers we seek you contribute for some 3-4 weeks and your contributions approved by existing maintainers
